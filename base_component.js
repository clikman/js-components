/**
 * Base component contains base functions for all components.
 * "constructor"-function should contains definition of all public variables.
 */

export class BaseComponent {
    constructor({containerSelector='', initValue='', options={}, callbacks={}, values={}}) {
        this.options = options;
        this.containerElement = null;  // root-element for the rendering. It should be rendered further.
        this.callbacks = {};  // dictionary: key - event's name, value - event's handler
        this.containerSelector = containerSelector;
    }

    generateString(len=10) {
        return Math.random(len, len).toString(36).replace(/[^a-z]+/g, '').substr(0, len);
    }

    generateID() {
        return this.generateString(15);
    }

    renderHTMLElement(tagName, attributes={}, text='') {
        var element = document.createElement(tagName);
        for(var attrName in attributes) {
            element.setAttribute(attrName, attributes[attrName]);
        }
        if (text) {
            element.textContent = text;
        }
        element.getValue = this.getValue;
        return element;
    }

    hangEventsOnElement(element, events) {
        for(var eventName in events) {
            element[eventName] = events[eventName].bind(null, element, this);
        }
        return element;
    }

    handEvent(element, eventName, eventHandler, options={}) {
        element.addEventListener(eventName, eventHandler, options);
        return element;
    }

    createEvent(eventName, detail={}, bubbles=true, cancelable=true) {
        return new CustomEvent(eventName, {'bubbles': bubbles, 'cancelable': cancelable, 'detail': detail});
    }

    render(containerSelector) {
        return document.querySelector(containerSelector).append(this.containerElement);
    }

    /**
     * The method should return "a value" for the component.
     * It may be what you want for each component.
     */
    setValue(value) {}

    /**
     * The method should return "a value" for the component.
     * It may be what you want.
     */
    getValue(){ return null; }

    showErrorMsg(node, text) {
        node.classList.add('error');
        node.classList.remove('success');
        node.innerHTML = text;
        node.style = 'display:block';
    }

    hideErrorMsg(node) {
        node.innerHTML = '';
        node.style = 'display:none';
    }

    showMsg(node, text) {
        node.classList.add('success');
        node.classList.remove('error');
        node.innerHTML = text;
        node.style = 'display:block';
    }

    hideMsg(node) {
        this.hideErrorMsg(node);
    }

    getHTML() {
        return this.containerElement.outerHTML;
    }
}
