/**
* Component for displaying URL-field.
*/

import { BaseComponent } from './base.js';

export class Textarea extends BaseComponent {
    constructor({containerSelector='body', initValue='', options={}, callbacks={}, values={}}) {
        super(...arguments);
        this.isValid = false;
        this.msgElement = undefined;
        this.build(containerSelector, options, callbacks);
        if (containerSelector) {
            this.render(containerSelector);
        }
        this.setValue(initValue);
        return this;
    }

    build(containerSelector, options={}, callbacks={}) {
        this.callbacks = callbacks;
        var inputAttributes = {};
        inputAttributes['class'] = options['class'] || '';
        inputAttributes['id'] = options['id'] || this.generateID();
        inputAttributes['name'] = options['name'] || inputAttributes['id'];
        inputAttributes['placeholder'] = options['placeholder'] || 'Text...';
        inputAttributes['title'] = 'Please input text';

        this.containerElement = this.renderHTMLElement('div', {'class': 'our-component textarea-container',
            'error-msg': inputAttributes['title']});
        if (options['headerText']) {
            var header = this.renderHTMLElement('h5', {'class': 'textarea-header'});
            header.textContent(options['headerText']);
            this.containerElement.append(header);
        }
        this.element = this.renderHTMLElement('textarea', inputAttributes);
        if (options['handler']) {
            callbacks['onkeyup'] = this.doneTyping(options['handler'],
                options['timeout'] || 1000);
        }
        this.element = this.hangEventsOnElement(this.element, callbacks);
        this.containerElement.append(this.element);
        this.msgElement = this.renderHTMLElement('p', {'class': 'message hide'});
        this.element.after(this.msgElement);
    }

    setValue(value) {
        this.element.value = value;
        this.value = value;
        this.isValid = Boolean(value.length);
    }

    getValue() {
        return this.value || this.element.value;
    }

    clean() {
        this.element.value = '';
    }

    doneTyping(callback, timeout=1500) {
        var that = this;
        var typingTimer;

        return function() {
            var args = arguments;
            clearTimeout(typingTimer);
            if (that.element.value) {
                typingTimer = setTimeout(function() { that.setValue(that.element.value); callback(...args); }, timeout);
            }
        };
    }
}