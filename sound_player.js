/** SoundPlayer-component for playing the audio records. */

export class SoundPlayer {
    constructor(options) {
        this.player = options['player'] || Audio;
        try {
            this.src = options.src;
        } catch (e) {
            console.error('SRC-option is not set for the SoundPlayer-component');
        }
        this.options = options;
        this.build();
        return this;
    }

    build() {
        var audio = new this.player();
        var date = new Date();
        audio.src = this.src + '?' + date.getMilliseconds() + date.getSeconds();
        if (this.options['autoplay'])
            audio.autoplay = 'autoplay';
        if (this.options['loop'])
            audio.loop = 'loop';
        if (this.options['controls'])
            audio.controls = true;
        this.audio = audio;
        this.audio.load();
    }

    async play() {
        this.audio.currentTime = 0;
        try {
            await this.audio.play();
        } catch (e) {
            console.error('SoundPlayer has a problem!');
        }
    }

    stop() {
        this.audio.pause();
    }
}